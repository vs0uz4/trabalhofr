/* 
Exercicio 03
ler e armazenar em um vetor a temperatura média de todos os dias do ano
*/

#include <stdio.h>

int main() { 
    // declarando as variáveis
    float temperatura[365];
    int i=0, dias=0;
    float maior, menor, media;
   
    // lendo as temperaturas
    for (i=0; i<=364; i++) {
        printf("Temperatura do dia %d: ", i + 1);
        scanf("%f", &temperatura[i]);
        
        if (i==1){
            maior = temperatura[i];
            menor = temperatura[i];
        }
        
        if (maior<temperatura[i]){
            maior = temperatura[i];
        }
        
        if (menor>temperatura[i]){
            menor = temperatura[i];
        }
        
        media+=temperatura[i];
    }
    
    // Calculando a temperatura média
    media = media/365;
    
    // Contando dias abaixo da média
    for (i=0; i<=364; i++){
        if (temperatura[i]<media){
            dias++;
        }
    }
    
    // imprimindo os resultados
    printf("\nMenor Temperatura do Ano : %.2f c\n", menor);
    printf("\nMaior Temperatura do Ano : %.2f c\n", maior);
    printf("\nTemperatura Média Anual : %.2f c\n", media);
    printf("\nNúmero de Dias no Ano, com Temperatura abaixo da Média Anual : %u\n", dias);
    return 0;  
}
