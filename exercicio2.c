/* 
Exercicio 02
Faça um programa de consulta pela posição numérica da pessoa: leia nomes de pessoas, sendo a quantidade determinada pelo usuário. Logo após a entrada pergunte ao usuário o número do nome que ele gostaria de consultar. Após sua resposta, exiba o nome que fica na posição informada. Chame atenção do usuário em caso de uma consulta inválida, ou seja, com números menores ou iguais a zero, ou maiores do que a quantidade cadastrada.
*/

#include <stdio.h>
#include <string.h> 

int main() { 
    // declarando as variáveis e inicializando-a
    int qtde=0, i=0, busca=0;

    // lendo a quantidade de pessoas a serem cadastradas
    printf("Quantas pessoas deseja cadastrar? : ");
    scanf("%d", &qtde);
    
    /* limpo o buffer do lixo deixado pelo scanf */
	getchar();
    
    // verificando quantidade informada
    if (qtde > 0){
        char pessoas[qtde][150 + 1];
        
        for (i=0; i < qtde; i++) {
            printf("Entre com o nome da pessoa %d: ", i + 1);
            fgets(pessoas[i], 150 + 1, stdin);
        }
        printf("Entre com o número do nome a consultar: ");
        scanf("%d", &busca);
        
        if ( (busca<=0) || (busca>qtde) ) {
            printf("Registro não encontrado, o número informado é inválido!\n");
        }else{
            printf("Nome referente ao registro %d é : %s ", busca, pessoas[busca-1]);   
        }
        
    }else{
        printf("A quantidade informada é inválida!\n");
    }
    
    return 0;  
}
