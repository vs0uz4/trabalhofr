/* 
Exercicio 04
Dados dois números naturais m e n e duas seqüências ordenadas com m e n números inteiros, obter uma única seqüência ordenada contendo todos os elementos das seqüências originais sem repetição.
*/

#include <stdio.h> 
#include <stdlib.h>

int main() { 
   // declarando variáveis
   int *veta, *vetb, *vetc, m, n, i, aux, k=0; 
   char troca = 'S'; 
    
   // coletando informações do primeiro vetor
   printf("Informe a quantidade de elementos do primeiro vetor: "); 
   scanf("%d", &m); 
   veta = (int *) malloc(m * sizeof(int)); 
   for (i=0; i<m; i++) { 
      printf("Informe veta[%d]: ", i); 
      scanf("%d", &veta[i]); 
   } 
   
   // coletando informações do segundo vetor
   printf("Informe a quantidade de elementos do segundo vetor: "); 
   scanf("%d", &n); 
   veta = (int *) malloc(n * sizeof(int)); 
   for (i=0; i<m; i++) { 
      printf("Informe vetb[%d]: ", i); 
      scanf("%d", &vetb[i]); 
   } 
    
   // criando vetor para alocar os 2 vetores informados
   vetc = (int *) malloc((m+n) * sizeof(int)); 
   for (i=0; i<m; i++) 
      vetc[i] = veta[i]; 
   for (i=0; i<n; i++) 
      vetc[m+i] = vetb[i]; 
    
   // ordenando o vetor
   while (troca == 'S') { 
      troca = 'N'; 
      for (i=0; i<(m+n-1); i++) { 
         if (vetc[i] > vetc[i+1]) { 
            aux = vetc[i]; 
            vetc[i] = vetc[i+1]; 
            vetc[i+1] = aux; 
            troca = 'S'; 
         } 
      } 
   } 
   k = m + n; 
   i=0; 
   while(i<k) { 
      while (vetc[i] == vetc[i+1]) { 
         vetc[i] = vetc[i+1]; 
         k--; 
         i++; 
      } 
      i++; 
   } 
    
   // imprimindo o resultado
   printf("\nResultado:\n"); 
   for (i=0; i<k; i++) 
      printf("\t%d", vetc[i]);
      printf("\n");
   return 0; 
}
