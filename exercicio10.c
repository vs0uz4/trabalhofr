/* 
Exercicio 10
Implemente um programa que receba uma linha de texto, retire os espaços em excesso existentes deixando apenas um espaço entre as várias palavras.
*/

#include <stdio.h>
#include <string.h>

int main(){
    // declarando variáveis  
    char texto[500];
    int i,j;
    
    // recebendo texto a ser formatado
    printf("Digite um texto com espacos\n");
    fgets(texto, 500, stdin);

    // imprimindo o texto digitado sem formatacao
    printf("\n Texto digitado: %s\n", texto);
    
    // formatando o texto e removendo os espaços desnecessários
    for(i=0; i<strlen(texto)-1; i++)
    {
        if(texto[i]==' ' && texto[i+1]==' ')
        {
            while(texto[i+1]==' ')            
            {
                for(j=i+1; j<strlen(texto)-1; j++)
                    texto[j] = texto[j+1];                
                texto[j]='\0';
            }
        }
    }
    
    // imprimindo o resultado
    printf("\n Texto formatado: %s", texto);      
    
    return 0;
}
