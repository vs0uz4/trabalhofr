/* 
Exercicio 01
Dados dois strings (um contendo uma frase e outro contendo uma palavra), determine o número de vezes que a palavra ocorre na frase.
*/

#include <stdio.h>
#include <string.h> 

int main() { 
   // declarando as variáveis
   char str_frase[255], str_palavra[20], *busca; 
   int tamanho_frase, tamanho_palavra, contador=0; 
   
   // lendo a variavel str_frase
   printf("Entre com uma frase: "); 
   fgets(str_frase, 255, stdin); 
   
   // atribuindo o tamanho da frase a variavel tamanho_frase
   tamanho_frase = strlen(str_frase); 
    
   // lendo a variavel str_palavra
   printf("Entre com a palavra: "); 
   scanf("%s", str_palavra);
    
   // atribuindo o tamanho da frase a variavel tamanho_frase
   tamanho_palavra = strlen(str_palavra);
    
   // buscando ocorrencias da palavra na frase
   busca = str_frase; 
   do { 
      if (strncmp(busca, str_palavra, tamanho_palavra) == 0) 
         contador++; 
      busca++; 
   } while (*(busca+tamanho_palavra-1) != '\0');

   // imprimindo resultado
   printf("\n'%s' ocorre %d vezes em '%s'\n", str_palavra, contador, str_frase); 
   return 0; 
} 
