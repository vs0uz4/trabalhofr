/* 
Exercicio 11
Uma locadora tem em um vetor de 50 posições a quantidade de filmes retirados pelos seus 50 clientes. A locadora esta fazendo um promoção que a cada 10 filmes retirados o cliente ganha uma locação gratuita, escreva um outro vetor mostrando quantas locações grátis cada um dos 50 clientes tem direito.
*/

#include <stdio.h>
#include <string.h>


int main() { 
    // declarando as variáveis
    int Filmes[50], FilmesGratis[50];
    int i, j;
    
    for (i=0; i<50; i++){
        printf("Quantidade de filmes à retirar : ");
        scanf("%u", &Filmes[i]);
        
        for (j=0; j<50; j++){
            FilmesGratis[j] = Filmes[i]/10;
        }
        if (Filmes[i] >= 10){
            printf("Quantidade de locações grátis : %d\n", FilmesGratis[i]);
            printf("Quantidade de filmes locados : %d\n\n", (Filmes[i]+FilmesGratis[i]) );
        }else{
            printf("Nenhuma locação gratuita!\n"); 
            printf("Quantidade de filmes locados : %d\n\n", Filmes[i]);  
        }
        
    }
    
    return 0;
}
