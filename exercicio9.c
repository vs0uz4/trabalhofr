/* 
Exercicio 9
Elaborar um algoritmo que lê um conjunto de 30 valores e os coloca em 2 vetores conforme os valores forem pares ou impares. O tamanho destes dois vetores é de 5 posições. Se algum vetor estiver cheio escrevê-lo. Terminada a leitura escrever o conteúdo dos dois vetores. Cada vetor pode ser preenchido tantas vezes quanto for necessário.
*/

#include <stdlib.h>
#include <stdio.h>

int main(){
    // declarando as variáveis  
    int par[5], impar[5];
    int i, numero, resto;
    
    printf("Escreva 10 números apertando ENTER a cada digitacao\n"); 
    for (i=0;i<=9;i++) { 
        scanf("%u", &numero);
        getchar();
        
        // verificando se número e par ou ímpar
        resto=numero%2; 
        if(resto==0){ 
            par[i]=numero;
        }else{
            impar[i]=numero; 
        } 
    }
    
    printf("Os numeros pares são : "); 
    for (i=0; i<=4; i++){
        printf ("%d, ", &par[i]);
    }
    
    printf("\nOs numeros impares são : "); 
    for (i=0; i<=4; i++){
        printf ("%d, ", &impar[i]);
    }
    printf ("\n");
    
    return 0;
} 
